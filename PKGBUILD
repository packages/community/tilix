# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Printzell <wild@archlinux.org>
# Contributor: Filipe Laíns (FFY00) <lains@archlinux.org>
# Contributor: dsboger <https://github.com/dsboger>
# Contributor: Carl George < arch at cgtx dot us >

pkgname=tilix
pkgver=1.9.6
pkgrel=6
pkgdesc="A tiling terminal emulator for Linux using GTK+ 3"
arch=('x86_64')
url="https://gnunn1.github.io/tilix-web"
license=('MPL-2.0')
depends=(
  'dconf'
  'gsettings-desktop-schemas'
  'gtkd'
  'liblphobos'
  'libx11'
  'vte3>=0.78.0'
)
makedepends=(
  'ldc'
  'meson'
  'po4a'
)
checkdepends=(
  'appstream'
)
optdepends=(
  'python-nautilus: for "Open Tilix Here" support in nautilus'
  'libsecret: for the password manager'
)
source=("https://github.com/gnunn1/tilix/archive/$pkgver/$pkgname-$pkgver.tar.gz"
        "https://github.com/gnunn1/tilix/pull/2222.patch"
        "https://github.com/gnunn1/tilix/commit/69fe457b58b58eb6f679bc50ef040d08b40fb65d.patch")
sha256sums=('be389d199a6796bd871fc662f8a37606a1f84e5429f24e912d116f16c5f0a183'
            'e56da87ae09124a229b5641de5a477351db9656063def9ad07f42b51735bd826'
            'a95698f97883fab52de39fb6b7f8d5728bf2bdf04f7d703ee630a9a154a105ee')

prepare() {
  cd $pkgname-$pkgver

  # Remove unneeded libunwind dep
  patch -Np1 -i ../2222.patch

  # Fix test failure
  # metainfo: Add a developer-id
  patch -Np1 -i ../69fe457b58b58eb6f679bc50ef040d08b40fb65d.patch
}

build() {

  # Build with LDC
  export DC=ldc
  export LDFLAGS="$(echo -ne $LDFLAGS | sed -e 's/-flto=auto//')"
  export DFLAGS="--flto=full --allinst"

  arch-meson "$pkgname-$pkgver" build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs

  appstreamcli validate --no-net build/data/com.gexperts.Tilix.appdata.xml
}

package() {
  meson install -C build --destdir "$pkgdir"
}
